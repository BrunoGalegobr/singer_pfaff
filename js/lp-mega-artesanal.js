//Início - Mega artesanal Formulario //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function(){
	$('#form-ma').find('#telefone-ma').mask('(99) 99999-9999');

	$('form#form-ma').on('submit', function(){
		var jsonData = JSON.stringify({
			'nome': $('form#form-ma input#nome-ma').val(),
			'email': $('form#form-ma input#email-ma').val(),
			'telefone': $('form#form-ma input#telefone-ma').val(),
			'genero': $('form#form-ma #genero-ma').find(':selected').val(),
			'nivelcostura': $('form#form-ma #nivel-costura-ma').find(':selected').val()
		});

		$.ajax({
			url: '//api.vtexcrm.com.br/'+jsnomeLoja+'/dataentities/MA/documents',
			type: 'POST',
			dataType: 'json',
			data: jsonData,
			headers: {
				"accept": "application/json",
				"content-type": "application/json"
			},
			// beforeSend: function() {
			// 	// $('input#enviar-form-mega').val('Enviando...').attr('disabled', true).css('opacity', '0.8');
			// 	console.log('enviando')
			// },
			success: function() {
				// $('input#enviar-form-mega ').val('Enviado').removeAttr('style');
				//    alert('Cadastro realizado com sucesso!');
				console.log('enviado')
				Swal.fire({
					type: 'success',
					title: 'O seu cadastro foi efetuado com sucesso!',
					text: 'Em breve você receberá nossas ofertas e promoções!',
					confirmButtonText: 'OK',
					confirmButtonColor: '#019dde',
				});
				$('form#form-ma input#nome-ma').val(''),
				$('form#form-ma input#email-ma').val(''),
				$('form#form-ma input#telefone-ma').val('')	
				
			},
			error: function(e){
				// $('input#enviar-form-mega').val('Enviar').removeAttr('disabled').removeAttr('style');
				//  alert('Não foi possível cadastrar. Por favor, tente novamente.');
				
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'O seu cadastro não foi efetuado. Por favor, verifique os dados inseridos.',
					confirmButtonText: 'OK',
					confirmButtonColor: '#019dde',
				});
			},   
		});
		return false;
	});
});
//Fim - Mega artesanal Formulario///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
