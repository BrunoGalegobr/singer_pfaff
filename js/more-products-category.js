$(".vitrine[id*=ResultItems]").QD_infinityScroll({
	paginate: function (moreResults) {
		$(".loadProd").click(moreResults);
	}
});

$(window).bind("QuatroDigital.is_noMoreResults", function() {
	$(".loadProd").after("<div class='noResults'><p>Não existem mais resultados</p></div>");
	$(".loadProd").hide();
});