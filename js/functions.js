function cronometro() {

    $Relogio_texto_oferta = '<strong>50%</strong> DE DESCONTO';
    $Relogio_local = '.cronometro';
    $Relogio_texto_titulo1 = 'APROVEITE NOSSAS SUPER OFERTAS COM';
    $Relogio_texto_titulo2 = 'OFERTAS POR TEMPO LIMITADO';

    if (typeof ($Relogio_data_oferta) !== "object") {
        $Relogio_data_oferta = $Relogio_data_oferta.split("/");
        $Relogio_data_oferta = new Date($Relogio_data_oferta.reverse());
        $Relogio_data_oferta = $Relogio_data_oferta.setDate($Relogio_data_oferta.getDate() + 1);

    } else {
        $Relogio_data_oferta = $Relogio_data_oferta.setDate($Relogio_data_oferta.getDate() + 4);
    }

    if ($Relogio_data_oferta > new Date()) {
        $($Relogio_local).append(`

        <div class="counter-offer">
            <div class="counter">
                <div class="counter-wrap">
                    <div class="item days" data-value=""><span class="description">Dias</span></div>
                    <div class="item hours" data-value=""><span class="description">Horas</span></div>
                    <div class="item minutes" data-value=""><span class="description">Min</span></div>
                    <div class="item seconds" data-value=""><span class="description">Seg</span></div>
                </div>
            </div>
        </div>

        `);
        counter($Relogio_data_oferta);
    }

    function counter(date) {
        var target_date = new Date(date).getTime();
        var days, hours, minutes, seconds;

        setInterval(function () {

            var current_date = new Date().getTime();
            var seconds_f = (target_date - current_date) / 1000;

            days = parseInt(seconds_f / 86400);
            seconds_f = seconds_f % 86400;

            hours = parseInt(seconds_f / 3600);
            seconds_f = seconds_f % 3600;

            minutes = parseInt(seconds_f / 60);
            seconds = parseInt(seconds_f % 60);

            if (target_date >= current_date) {
                $('.days').attr("date-value", days < 10 ? "0" + days : days);
                $('.hours').attr("date-value", hours < 10 ? "0" + hours : hours);
                $('.minutes').attr("date-value", minutes < 10 ? "0" + minutes : minutes);
                $('.seconds').attr("date-value", seconds < 10 ? "0" + seconds : seconds);
            }

            if (days == 0 && hours == 0 && minutes == 0 && seconds == 00) {
                $('.offers').remove();
            }

        }, 1000);
    }

    $('.counter-offer').before($('.vit-primeira h2'));

}

function slideBeneficios() {
    $('section.slideBeneficios .container ul').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 7,
        prevArrow: '<div class="bf-shelf__button bf-shelf__button--previous"><svg class="icon icon-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use></svg></div>',
        nextArrow: '<div class="bf-shelf__button bf-shelf__button--next"><svg class="icon icon-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg></div>',
        slidesToScroll: 7,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }
        ]
    });

}

$(document).ready(function () {


    // only home:
    cronometro();
    slideBeneficios();


});