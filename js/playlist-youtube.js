const YPlaylist = {
    init(config) {
        this.url = `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=${config.playlist}&key=${config.apiKey}&callback=?`;
        this.container = config.container;
        this.shuffle = config.shuffle;
        this.fetch();
    },
    fetch() {
        const self = this;
        const carousel = $('<div class="slick-track" id="container-playlist"></div>');

        $.getJSON(self.url, data => {
            let list = "";
            let res = {};
            const entries = data.items;
            let i;
            let len;

            if (self.shuffle) {
                entries.sort(() => 0.5 - Math.random());
            }
            console.log(entries.length);
            for (i = 0; i < entries.length; i += 1) {
                console.log(entries[i]);
                res = {
                    title: entries[i].snippet.title,
                    url: entries[i].snippet.resourceId.videoId,
                    thumb: entries[i].snippet.thumbnails.maxres.url,
                    desc: entries[i].snippet.description
                };

                list += '<div class="slider__slide"><div class="video"><div class="video__overlay">';
                list += `<div class="video__image" style="background-image: url(${res.thumb});"></div><!-- /.video__media -->`;
                list += `<div class="video__actions"><a href="https://www.youtube.com/watch?v=${res.url}" target="_blank" class="btn btn--width btn--pink btn--pink-solid btn--ico video__btn" tabindex="0"><i class="ico-play-large"></i><p>${res.title}</p></a></div><!-- /.video__content -->`;
                list += '</div><!-- /.video-preview --></div></div>';
            }

            $(self.container).append(carousel);
            $(list).appendTo('#container-playlist');
        });
        $('.video-preview .video__btn').attr('target', '_blank');
    },
    truncate(text, limit) {
        let last;
        if (text.length > limit) {
            limit--;
            last = text.substr(limit - 1, 1);
            while (last !== ' ' && limit > 0) {
                limit--;
                last = text.substr(limit - 1, 1);
            }
            last = text.substr(limit - 2, 1);
            if (last === ',' || last === ';' || last === ':') {
                text = `${text.substr(0, limit - 2)}...`;
            } else if (last === '.' || last === '?' || last === '!') {
                text = text.substr(0, limit - 1);
            } else {
                text = `${text.substr(0, limit - 1)}...`;
            }
        }
        return text;
    },
    getId(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
        const match = url.match(regExp);

        if (match && match[2].length === 11) {
            return match[2];
        } else {
            throw new Error("Invalid video URL");
        }
    },
};