var $win = $(window);

$win.on('load', function () {
    /* Scroll menu flutuante */
    $(window).scroll(function(){
        var top =  $(window).scrollTop();
        if(top > $('.header__inner').height() + 30) {
            $('.header').addClass('header-flutuante');
        } else {
            $('.header').removeClass('header-flutuante');
        };
    });

    /* Scroll top */
    $('body').on('click', 'a.to-top', function () {
        $("html, body").animate({
            scrollTop: 0
        }, "slow")
    });

    $win.on('scroll', function () {
        var scroll = $(this).scrollTop();
        if (scroll > 100) {
            $('a.to-top').fadeIn();
        } else {
            $('a.to-top').fadeOut();
        }
    });

    // Textos de SEO - Descritivos de categoria
    $('.show-seo-content').on('click', function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).text('Veja mais');
            $(this).prev('.hide-seo-content').slideUp();
        } else {
            $(this).addClass('active');
            $(this).text('Veja menos');
            $(this).prev('.hide-seo-content').slideDown();
        }
    });

    // Porcentagem em desconto
    function applyDiscount() {
        $('.dsct-pct').each(function(){
            if (!$(this).hasClass('discount-loaded')) {
                var dsct = $(this).text();
                dsct = Math.round(dsct.replace(",", ".").replace("%", "")) + '% OFF';
                $(this).text(dsct).show().addClass('discount-loaded');
            }
        });
    }

    applyDiscount();

    // Selos de natal Singer
    function applyChristmasFlag() {
        $('.natal-2018').each(function(){
            if (!$(this).hasClass('flag-loaded')) {
                $(this).addClass('flag-loaded').parents('.product').prepend('<span class="christmas-flag"></span>');
            }
        });
    }

    applyChristmasFlag();

    // Passo a passo de compra
    function buyHelper() {
        const buyBtn = 
        '<p class="buyhelper-init">' + 
            '<span><strong>Tutorial</strong> - Como comprar no site?</span>' + 
        '</p>';

        const buyContent =
        '<div class="buyhelper-fade"></div>' +
        '<div class="buyhelper-content">' +
            '<div class="buyhelper-box to-top right" id="step-1">' +
                '<span>X</span>' +
                '<h2>01.</h2>' +
                '<h3>Olá, bem-vindo</h3>' +
                '<p>Caso você já tenha cadastro ou já fez alguma compra em nosso site, basta fazer o seu login. Se você ainda não é cliente da loja virtual, solicite o envio da chave de acesso e em seguida preencha seu dados.</p>' +
                '<button id="tostep-2">Ok! Entendi</button>' +
            '</div>' +
            '<div class="buyhelper-box to-top center" id="step-2">' +
                '<span>X</span>' +
                '<h2>02.</h2>' +
                '<h3>Busca</h3>' +
                '<p>Caso você já saiba qual produto deseja, basta informá-lo no campo de busca.</p>' +
                '<button id="tostep-3">Ok! Entendi</button>' +
            '</div>' +
            '<div class="buyhelper-box to-top left" id="step-3">' +
                '<span>X</span>' +
                '<h2>03.</h2>' +
                '<h3>Departamento</h3>' +
                '<p>Você pode encontrar os produtos que deseja navegando pelo menu de departamentos.</p>' +
                '<button id="tostep-4">Ok! Entendi</button>' +
            '</div>' +
            '<div class="buyhelper-box to-bottom left" id="step-4">' +
                '<span>X</span>' +
                '<h2>04.</h2>' +
                '<h3>Ver produtos</h3>' +
                '<p>Ao clicar no produto, você será redirecionado para a página de detalhes deste produto, como descrição, acessórios, voltagem, conteúdo.</p>' +
                '<button id="tostep-5">Ok! Entendi</button>' +
            '</div>' +
            '<div class="buyhelper-box to-top right" id="step-5">' +
                '<span>X</span>' +
                '<h2>05.</h2>' +
                '<h3>Carrinho</h3>' +
                '<p>Os produtos adicionados no carrinho estarão aqui.</p>' +
                '<button id="tostep-6">Ok! Entendi</button>' +
            '</div>' +
            '<div class="buyhelper-box to-bottom left" id="step-6">' +
                '<span>X</span>' +
                '<h2>06.</h2>' +
                '<h3>Precisa de ajuda?</h3>' +
                '<p>Veja aqui informações, política de trocas, devoluções e entrega.</p>' +
                '<button id="tostep-0">Ok! Entendi</button>' +
            '</div>' +
        '</div>';

        const elShelf       = '.section.section--primary:first .slick-initialized.slick-slider';
        const elCart1       = '.nav-utilities.header__nav ul li:last-of-type';
        const elCart2       = '.nav-utilities.header__nav ul li:last-of-type .mini-cart.col-mini-cart';
        const elCart3       = '.nav-utilities.header__nav ul li:last-of-type .mini-cart.col-mini-cart .ico-heart';
        const elSearch      = '.search.header__search';
        const elPolicy      = '.footer__inner .cols.accordions .col:first-of-type';
        const elAccount     = '.nav-utilities.header__nav ul li:nth-child(3) a';
        const elDepartament = '.header__bar .accordions li:first-of-type';

        $('.home .header').prepend(buyBtn);

        // Step 1
        $('body').on('click', '.buyhelper-init span',  function() {
            $('body.home').prepend(buyContent);
            
            $(elAccount).css({
                'z-index'       : 9999,
                'padding'       : '10px',
                'position'      : 'relative',
                'background'    : '#ffffff',
                'border-radius' : '20px'
            }).addClass('no-style');

            $('.buyhelper-fade').fadeIn(300, function() {
                $('#step-1').addClass('active');
            });
        });

        // Step 2
        $('body').on('click', '#tostep-2', function() {
            $('#step-1').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $(elSearch).css({
                'z-index'       : 9999,
                'position'      : 'relative'
            }).addClass('no-style');

            $('#step-2').addClass('active');
        });

        // Step 3
        $('body').on('click', '#tostep-3', function() {
            $('#step-2').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $(elDepartament).css({
                'z-index'        : 9999,
                'padding'        : '0 20px',
                'position'       : 'relative',
                'background'     : '#ffffff',
                'border-radius'  : '30px',
                'pointer-events' : 'none'
            }).addClass('no-style');

            $('#step-3').addClass('active');
        });

        // Step 4
        $('body').on('click', '#tostep-4', function() {
            $('#step-3').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $(elShelf).css({       
                'z-index'       : 9999,
                'padding'       : '20px',
                'position'      : 'relative',
                'position'      : 'relative',
                'background'    : '#ffffff',
                'border-radius' : '30px'
            }).addClass('no-style');

            $('html, body').animate({
                'scrollTop' : 2520
            }, 800, function(){
                $('#step-4').addClass('active'); 
            });
        });

        // Step 5
        $('body').on('click', '#tostep-5', function() {
            $('#step-4').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $(elCart1).css({
                'width'         : '51px',
                'margin'        : '-14px 0 0 25px',
                'height'        : '51px',
                'z-index'       : 9999,
                'padding'       : '15px',
                'background'    : '#ffffff',
                'border-radius' : '50%'
            }).addClass('no-style');

            $(elCart2).css({
                'margin-top'  : '5px',
                'margin-left' : '-5px'
            }).addClass('no-style');

            $(elCart3).css('right', '5px').addClass('no-style');

            $('html, body').animate({
                'scrollTop' : 0
            }, 800, function(){
                $('#step-5').addClass('active'); 
            });
        });

        //Step 6
        $('body').on('click', '#tostep-6', function() {
            $('#step-5').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $(elPolicy).css({
                'z-index'      : 9999,
                'padding'      : '10px 0px 10px 20px',
                'position'     : 'relative',
                'background'   : '#ffffff',
                'margin-right' : '20px'
            }).addClass('no-style');

            $('html, body').animate({
                'scrollTop' : 6300
            }, 800, function(){
                $('#step-6').addClass('active'); 
            });
        });

        //Step 7 and close
        $('body').on('click', '.buyhelper-box span, #tostep-0', function() {
            $('.buyhelper-box.active').removeClass('active');
            $('.no-style').removeAttr('style').removeClass('no-style');
            
            $('.buyhelper-fade').fadeOut(function(){
                $(this).remove();
            });

            $('html, body').animate({
                'scrollTop' : 0
            }, 800);
        });
    }

    if ($('body').hasClass('home') && window.location.href.indexOf("singer") > -1) {
        buyHelper();
    }
});

