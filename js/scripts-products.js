/* Scripts da loja */
var $ = $;
var wdw = $(window);
var body = $('body');

var codeEplusAlpha = {

	removeVoltTitleProduto: function () {
		$('.product__title .productName').each(function () {
			var title_prod = $(this).text().replace('- 127V', '').replace('- 220V', '');
			$(this).text(title_prod);
		});
	},

	selo_OFF: function () {
		$('.info__aside p.flag').each(function (index, el) {
			var discount = $(this);
			var percent = $(this).text();
			percent = percent.replace(',', '.');
			percent = percent.replace(' %', '');
			percent = parseInt(percent);
			//////console.log(percent);
			$(this).html(percent + '% Off');
			//$('.productPrice .descricao-preco').append('<p class="desconto-boleto"><strong> -'+percent+'%</strong> no boleto</p>');
			$(this).addClass('complete');
			if (percent === 0) {
				discount.hide();
			}
		});
	}, //vtex_selo_OFF

	corta_titulo: function () {

		var nomeProd, nomeCount, alterNomeProd;
		var qtsChars = [];

		if ($('body').hasClass('mobilePage')) {
			qtsChars[0] = 82;
		} else {
			qtsChars[0] = 100;
		}

		qtsChars[1] = qtsChars[0] - 3;

		$('.product__head .productDescription, .info .productDescription').each(function (i) {

			nomeProd = $(this).html().trim();

			nomeProd = '<ul>' + nomeProd + '</ul>';

			nomeCount = nomeProd.length;

			if (nomeCount > qtsChars[0]) {

				alterNomeProd = nomeProd.slice(0, qtsChars[1]).trim();

				$(this).html(alterNomeProd + '...');

			}

		});

		$('.accordion__body.tab__body .textbox .productDescription').each(function () {

			descProduct = $(this).html().trim();
			descProduct = '<ul>' + descProduct + '</ul>';

			$(this).empty();

			$(this).append(descProduct);

		});

		//$('.product__head .productDescription, .info .productDescription').css('display', 'block');
	},

	compreJuntoVazio: function () {
		//console.log($('#divCompreJunto').height());
		if ($('#divCompreJunto').height() < 1) {
			$('.section--combination').hide();
		}
	},

	buttonTouchPoint: function () {
		var newButton = $('.quantity-outer').html();
		$('.section-bar .plugin-preco').after('<div class="quantity-outer touchpoint">' + newButton + '</div>');

		var offset = $('.info__section .form-alt').offset().top,
			touchPoint = $('.wrapper.wrapper--product > .section-bar.visible-xs-block');

		$(document).on('scroll', function () {
			if (offset <= $(window).scrollTop()) {
				touchPoint.addClass('fixed');
			} else {
				touchPoint.removeClass('fixed');
			}
		});
	},

	sugestoesVazio: function () {
		if ($('.suggestions-products .slider__slides').height() < 1) {
			$('.suggestions-products').hide();
		}
	},

	quemviuVazio: function () {
		if ($('.suggestions-products .slider__slides').height() < 1) {
			$('.quem-viu-viu-tambem').hide();
		}
	},

	camposEspecificacao: function () {
		if ($('#caracteristicas td.value-field.Conteudo-Embalagem').length > 0) {
			var conteudo_embalagem = $('#caracteristicas td.value-field.Conteudo-Embalagem').html();
			$('.accordion__body .box-conteudo-embalagem').html(conteudo_embalagem);
		}

		// Caracteristicas com Icones
		if ($('#caracteristicas td.value-field.Texto-Descritivo-1').length > 0) {
			var title_caracteristica1 = $('#caracteristicas th.name-field.Texto-Descritivo-1').text();
			var conteudo_caracteristica1 = $('#caracteristicas td.value-field.Texto-Descritivo-1').text();
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica1 + '</h5><p>' + conteudo_caracteristica1 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Texto-Descritivo-2').length > 0) {
			var title_caracteristica2 = $('#caracteristicas th.name-field.Texto-Descritivo-2').text();
			var conteudo_caracteristica2 = $('#caracteristicas td.value-field.Texto-Descritivo-2').text();
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica2 + '</h5><p>' + conteudo_caracteristica2 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Texto-Descritivo-3').length > 0) {
			$('.section.section--features').css('display', 'block');
			var title_caracteristica3 = $('#caracteristicas th.name-field.Texto-Descritivo-3').text();
			var conteudo_caracteristica3 = $('#caracteristicas td.value-field.Texto-Descritivo-3').text();
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica3 + '</h5><p>' + conteudo_caracteristica3 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Texto-Descritivo-4').length > 0) {
			var title_caracteristica4 = $('#caracteristicas th.name-field.Texto-Descritivo-4').text();
			var conteudo_caracteristica4 = $('#caracteristicas td.value-field.Texto-Descritivo-4').text();
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica4 + '</h5><p>' + conteudo_caracteristica4 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Texto-Descritivo-5').length > 0) {
			var title_caracteristica5 = $('#caracteristicas th.name-field.Texto-Descritivo-5').text();
			var conteudo_caracteristica5 = $('#caracteristicas td.value-field.Texto-Descritivo-5').text();
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica5 + '</h5><p>' + conteudo_caracteristica5 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Tipo-de-ponta').length > 0) {
			var title_caracteristica6 = $('#caracteristicas th.name-field.Tipo-de-ponta').text();
			var conteudo_caracteristica6 = $('#caracteristicas td.value-field.Tipo-de-ponta').text();
			$('.tab__body .name-type-product').text(title_caracteristica6);
			$('.tab__body .maquina-product').text(conteudo_caracteristica6);
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica6 + '</h5><p>' + conteudo_caracteristica6 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Conteudo-Embalagem').length > 0) {
			$('.section.section--features').css('display', 'block');
			var title_caracteristica7 = $('#caracteristicas th.name-field.Conteudo-Embalagem').text();
			var conteudo_caracteristica7 = $('#caracteristicas td.value-field.Conteudo-Embalagem').text();
			$('.tab__body .name-conteudo-embalagem').text(title_caracteristica7);
			$('.tab__body .conteudo-embalagem-product').text(conteudo_caracteristica7);
			$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">' + title_caracteristica7 + '</h5><p>' + conteudo_caracteristica7 + '</p></div></div>');
		}

		if ($('#caracteristicas td.value-field.Playlist--Video').length > 0) {
			var cod_playlist = $('#caracteristicas td.value-field.Playlist--Video').text();
			console.log(cod_playlist);
			YPlaylist.init({
				playlist: cod_playlist,
				apiKey: 'AIzaSyCTWXTYYIJaKZK6Ful8qpz0b8WoIKZy7w8',
				container: $('#playlist-youtube'),
				shuffle: false
			});
			$('.section-playlist').show();
		}
	},

	compreJuntoTextAdd: function () {
		if ($('#divCompreJunto').height() > 1) {
			$('.buy-together-content td.buy').prepend('<h2 class="box__title"><small>VocÃª leva</small>Os dois<br> produtos por:</h2>');
		}
	},

	produtoVoltagem: function () {
		var sku_id = $('#___rc-p-sku-ids').val();
		$.ajax({
			headers: {
				"Accept": "application/vnd.vtex.pricing.v3+json",
				"X-VTEX-API-AppKey": "vtexappkey-singer-NFZYKP",
				"X-VTEX-API-AppToken": "ZXHGFDEUCJCQKIGITPDXUMVOJDCFQKMIGMVHWRVEXEYFSSWPQJYPJMVNCTWCAZERWTJXNYMBTZZQNPVRGMOHLWZTRHFDVTDYZGJYIYCSWXWKMEZMXFVNKXGZUQGFSJFA"
			},
			url: '/api/catalog_system/pvt/sku/stockkeepingunitbyid/' + sku_id,
			type: 'GET',
			dataType: 'json',
			cache: false,
			contentType: "application/json; charset=utf-8",
			success: function (data) {
				//console.log(data.ProductSpecifications);
				var i, ean, product_similar, voltagem, diametro, especification, modelo, altura, largura, peso, comprimento, potencia;
				ean = data.AlternateIds.Ean;
				especification = data.ProductSpecifications;
				altura = data.Dimension.height;
				largura = data.Dimension.width;
				peso = parseFloat(data.Dimension.weight / 1000);
				comprimento = data.Dimension.length;
				i = 0;
				while (i < especification.length) {
					if (especification[i].FieldId == 20) {
						voltagem = especification[i].FieldValues[0];
						$('.info__section.radios-volts').css('display', 'block');
						$('.info__section.radios-volts ul.list-radios').append('<li class="selected"><div class="radio"><input type="radio" name="volts" id="' + sku_id + '"><label class="form__label" for="field-' + voltagem + '">' + voltagem + 'V</label></div></li>');
					}
					if (especification[i].FieldId == 37) {
						diametro = especification[i].FieldValues[0];
						$('.info__section.radios-diametro').css('display', 'block');
						$('.info__section.radios-diametro ul.list-radios').append('<li class="selected"><div class="radio"><input type="radio" name="diametro" id="' + sku_id + '"><label class="form__label" for="field-' + diametro + '">' + diametro + '</label></div></li>');
					}
					if (especification[i].FieldId == 21) {
						modelo = especification[i].FieldValues[0];
					}
					if (especification[i].FieldId == 22) {
						potencia = especification[i].FieldValues[0];
					}
					i++;
				}
				$('.tab__body .maquina-product').text(modelo);
				$('.tab__body .ean-product').text(ean);
				$('.tab__body .altura-product').text(altura + ' cm');
				$('.tab__body .largura-product').text(largura + ' cm');
				$('.tab__body .peso-product').text(peso + ' kg');
				$('.tab__body .comprimento-product').text(comprimento + ' cm');
				$('.tab__body .potencia-product').text(potencia);
			},
			error: function (error) {
				console.log(error);
			},
		});
	},

	produtoSimilar: function () {
		$('.info').before('<div id="agulhas"><p id="selecione">Selecione outras opções de Diâmetro:</p></div>')

		var product_id = $('#___rc-p-id').val();
		 $.ajax({ 
					url: '/api/catalog_system/pub/products/crossselling/similars/' + product_id,
					method: "GET",
					headers: {
						"Accept": "application/json",
						"Content-Type": "application/json",
					}
					
				}).done(function(data) {
		
		console.log(data)
		
			 if(data.length > 0){
			 
		
		  $.each(data, function(i, res){ 
		   
			$('#agulhas').after('<ul class="list-radios-agulhas"><a href="'+res.link+'"><li class="radio"><label class="form__label">'+res.Diâmetro+'</label></li></a></ul>')
		   
		
		  })
		} 
		});



		var product_id = $('#___rc-p-id').val();
		$.ajax('/api/catalog_system/pub/products/crossselling/similars/' + product_id, {
			type: 'GET',
			dataType: 'json',
			error: function (jqXHR, textStatus, errorThrown) {
				console.log("AJAX Error: " + textStatus);
			},
			success: function (data, textStatus, jqXHR) {
				$('.popups .question__actions .no-volt').click(function () {
					location.href = data[0].link
				});

				var i, product_similar, voltagem, diametro;
				i = 0;
				while (i < data.length) {
					if (data[i].Voltagem != null) {
						$('.buy-button.buy-button-ref').click(function (e) {
							e.preventDefault();
							$('.popups #popup-comprar').toggleClass("open");
							var voltavem_popup = $('.bread-crumb .last').text();
							$('.popups #popup-comprar .question strong').text(voltavem_popup);
						});
						if (data[i].Voltagem[0] != false) {
							voltagem = data[i].Voltagem[0];
							product_similar = data[i].items[0].itemId;
							$('ul.list-radios-agulhas').css('display', 'none');
							$('.info__section.radios-volts').css('display', 'block');
							$('.info__section.radios-volts ul.list-radios').append('<li><div class="radio"><input type="radio" name="volts" id="' + product_similar + '"><label class="form__label" for="field-' + voltagem + '">' + voltagem + 'V</label></div></li>');
							$('.info__section.radios-volts ul.list-radios li').click(function () {
								location.href = data[0].link
							});
						};
					}
					if (data[i].diametro != null) {
						if (data[i].diametro[0] != false) {
							diametro = data[i].diametro[0];
							console.log(diametro);
							product_similar = data[i].items[0].itemId;
							$('.info__section.radios-diametro').css('display', 'block');
							$('.info__section.radios-diametro ul.list-radios').append('<li><div class="radio"><input type="radio" name="diametro" id="' + product_similar + '"><label class="form__label" for="field-' + diametro + '">' + diametro + '</label></div></li>');
						};
					}
					i++;
				}
			}
		});
	},

	descontoBoletoProduto: function () {
		var porcentagem_desconto = 0.05; //porcentagem de desconto no boleto

		var price_product = $(".skuBestPrice").first().text();
		var price_descont = $(".skuListPrice").text();

		if ($('.box-desconto-boleto').find('.desconto-boleto').height() < 1) {
			if ((price_product != null) && (price_product != '')) {
				if (porcentagem_desconto > 0) {
					price_product = price_product.replace("R$", "").replace('.', '').replace(',', '.');
					var new_price = parseFloat(price_product);
					// desconto boleto
					var val_desconto = parseFloat(new_price) * parseFloat(porcentagem_desconto);
					var desconto_boleto = new_price - val_desconto;
					var valor_final = desconto_boleto.toFixed(2);
					var size_valor = valor_final.toString().length;
					if ((size_valor > 1) && (size_valor <= 6)) {
						var desconto = 'R$ ' + valor_final.replace('.', ',');
					}
					if ((size_valor > 6) && (size_valor <= 9)) {
						var string_desconto_boleto = valor_final.toString();
						var sum_desconto = string_desconto_boleto.substr(0, size_valor - 6) + '.' + string_desconto_boleto.substr(size_valor - 6, 3) + ',' + string_desconto_boleto.substr(size_valor - 2, size_valor);
						var desconto = 'R$ ' + sum_desconto;
					} else {
						var desconto = 'R$ ' + valor_final.replace('.', ',');
					}
					// $('.productPrice .descricao-preco').append('<p><strong> -5%</strong> no boleto</p>');
					// $(".info__price .box-desconto-boleto").prepend('<span class="desconto-boleto">'+desconto+' no boleto</span>');
				}
			} else {
				if (porcentagem_desconto > 0) {
					price_descont = price_descont.replace("R$", "").replace('.', '').replace(',', '.');
					var new_price = parseFloat(price_descont);
					// desconto boleto
					var val_desconto = parseFloat(new_price) * parseFloat(porcentagem_desconto);
					var desconto_boleto = new_price - val_desconto;
					var valor_final = desconto_boleto.toFixed(2);
					var size_valor = valor_final.toString().length;
					if ((size_valor > 1) && (size_valor <= 6)) {
						var desconto = 'R$ ' + valor_final.replace('.', ',');
					}
					if ((size_valor > 6) && (size_valor <= 9)) {
						var string_desconto_boleto = valor_final.toString();
						var sum_desconto = string_desconto_boleto.substr(0, size_valor - 6) + '.' + string_desconto_boleto.substr(size_valor - 6, 3) + ',' + string_desconto_boleto.substr(size_valor - 2, size_valor);
						var desconto = 'R$ ' + sum_desconto;
					} else {
						var desconto = 'R$ ' + valor_final.replace('.', ',');
					}
					// $('.productPrice .descricao-preco').append('<p><strong> -5%</strong> no boleto</p>');
					// $(".info__price  .box-desconto-boleto").prepend('<span class="desconto-boleto">'+desconto+' no boleto</span>');
				}
			}
		}

	},

	init: function () {
		//Chamada das FunÃ§Ãµes
		codeEplusAlpha.removeVoltTitleProduto();
		codeEplusAlpha.selo_OFF();
		codeEplusAlpha.corta_titulo();
		codeEplusAlpha.compreJuntoVazio();
		codeEplusAlpha.buttonTouchPoint();
		codeEplusAlpha.sugestoesVazio();
		codeEplusAlpha.quemviuVazio();
		codeEplusAlpha.camposEspecificacao();
		codeEplusAlpha.compreJuntoTextAdd();
		codeEplusAlpha.produtoVoltagem();
		codeEplusAlpha.produtoSimilar();
		codeEplusAlpha.descontoBoletoProduto();
	}
}
$(document).ready(function () {
	codeEplusAlpha.init();

	//scroll description
	function scrollToAnchor(aid) {
		var aTag = $("a[name='" + aid + "']");
		$('html,body').animate({
			scrollTop: aTag.offset().top
		}, 'slow');
	}

	$("#link").click(function () {
		scrollToAnchor('description-more');
	});

	$('.popups .question__actions .yes-volt').click(function () {
		var id_sku = $('.product__title #___rc-p-sku-ids').val();
		var url_buy_product = "/checkout/cart/add?sku=" + id_sku + "&qty=1&seller=1&redirect=true&sc=1"
		location.href = url_buy_product
	});

	$('.popups .question__actions .no-volt').click(function () {
		$('.popups #popup-comprar').removeClass('open');
		var list_sku = $("ul.list-radios li").length;
		if (list_sku > 1) {
			$('ul.list-radios li').not('.selected').click();
		}
	});
});
$(document).ajaxStop(function () {
	$('.info__section.radios-volts ul.list-radios li').click(function () {
		$('ul.list-radios li').removeClass("selected");
		$(this).addClass("selected");
		var id_sku = $(this).find('input').attr('id');
		var url_buy_product = "/checkout/cart/add?sku=" + id_sku + "&qty=1&seller=1&redirect=true&sc=1"
		$('.buy-button-box a.buy-button-ref').attr("href", url_buy_product);
		var voltavem_popup = $(this).find('label').text();
		$('.popups #popup-comprar .question strong').text(voltavem_popup);
		//$('.popups #popup-comprar').toggleClass( "open" );
	});
	// $('.info__section.radios-volts ul.list-radios li').click(function() {
	// 	$('.popups #popup-comprar').toggleClass( "open" )
	// });

	$('.info__section.radios-diametro ul.list-radios li').click(function () {
		$('ul.list-radios li').removeClass("selected");
		$(this).addClass("selected");
		var id_sku = $(this).find('input').attr('id');
		var url_buy_product = "/checkout/cart/add?sku=" + id_sku + "&qty=1&seller=1&redirect=true&sc=1"
		$('.buy-button-box a.buy-button-ref').attr("href", url_buy_product);
	});

	if ($('.info__section.radios-volts ul.list-radios').height() > 1) {
		var voltagens = '';
		$('ul.list-radios label.form__label').each(function () {
			voltagens += $(this).text() + ' / ';
		});
		voltagens = voltagens.slice(0, -2);
		$('.tab__body .voltagem-product').text(voltagens);
	}

	if ($('.info__section.radios-diametro ul.list-radios').height() > 1) {
		var diametros = '';
		$('ul.list-radios label.form__label').each(function () {
			diametros += $(this).text() + ' / ';
		});
		diametros = diametros.slice(0, -2);
		$('.slider-features .slider__slides').append('<div class="slider__slide"><div class="feature"><div class="feature__image"><i class="ico-feature-one"></i></div><h5 class="feature__title">DiÃ¢metro</h5><p>' + diametros + '</p></div></div>');
		$('.tab__body .name-voltagem').text('DiÃ¢metro');
		$('.tab__body .voltagem-product').text(diametros);
	}



	
});
vtexjs.catalog.getCurrentProductWithVariations().done(function (product) {
	var avise = setInterval(function () {
		if ($('.info__section.radios-volts').is(':visible')) {
			var disponibilidade = product.skus[0].availablequantity;
			if (disponibilidade == 0) {
				$('.info__section.info__section--alt, .info__section.info__section--alt, .field-quantity1, .pull-left.box-qtd, .form__label.hidden-xs').hide();
			}
		} else {
			clearInterval(avise)
		}
	}, 1000);
});

