/* Scripts da loja */
var $ = $;
var wdw = $(window);
var body = $('body');

var codeEplusAlpha = {

	corta_titulo:function(){

		var nomeProd, nomeCount, alterNomeProd;
		var qtsChars = [];

		if ( $('body').hasClass('mobilePage')) {
			qtsChars[0] = 82;
		} else {
			qtsChars[0] = 100;
		}

		qtsChars[1] = qtsChars[0] - 3;

		$('.product__head .productDescription').each(function(i){

			nomeProd = $(this).html().trim();

			nomeCount = nomeProd.length;

			if ( nomeCount > qtsChars[0] ) {

				alterNomeProd = nomeProd.slice(0,qtsChars[1]).trim();

				$(this).html(alterNomeProd+'...');

			}

		});
		$('.product__head .productDescription').css('display', 'block');
	},

	init: function(){
		codeEplusAlpha.corta_titulo();
	}
}
$(document).ready(function(){
    codeEplusAlpha.init();
});