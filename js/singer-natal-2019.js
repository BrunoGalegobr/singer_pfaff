$(document).ready(function(){


// Acordions

    $('p.accordion-one').click(function(){
        $(this).parent().find('div.accordion-one').slideToggle("slow");
        $(this).toggleClass('rotate');
    });
    $('p.accordion-two').click(function(){
        $(this).parent().find('div.accordion-two').slideToggle("slow");
        $(this).toggleClass('rotate');
    });
    $('p.accordion-three').click(function(){
        $(this).parent().find('div.accordion-three').slideToggle("slow");
        $(this).toggleClass('rotate');
    });
    $('p.accordion-four').click(function(){
        $(this).parent().find('div.accordion-four').slideToggle("slow");
        $(this).toggleClass('rotate');
    });

   
// Carousel da Coleção

   /*  $('.internaEspecial ul').owlCarousel({
        items:4,
        center: true,
        loop:true,
        dots: false,
        nav:true,
        responsiveClass:true,
        responsive:{
			0:{
                items:2
			},
			600:{
               items:2
			}
        }
    }); */

    $('.internaEspecial .vitrine-store-installment>ul').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: false,
        infinite: true,
        arrows: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });




/* 
    $('.owl-pagination').hide(); 
    $('.prev').html("<img src='/arquivos/singer-nav-left-colecao.png'>");
    $('.next').html("<img src='/arquivos/singer-nav-right-colecao.png'>");
*/


// Formulário

    $('#form_news').on('submit', function(){

        var MD_Entity = 'NT';

        var jsonData = JSON.stringify({
            'nome' : $(this).find('#nome').val(),
            'email' : $(this).find('#email').val()
        });
        
        var alert = `
                    <div id="alert-modal" style="position: absolute;
                        position: absolute;
                        top: 40%;
                        left: 40%;
                        background-color: #fff;
                        width: 407px;
                        height: 170px;
                        text-align: center;
                        padding: 65px 7px;
                        border-radius: 5px;
                        font-size: 22px;">

                        <span class="close" style="position: absolute;top: 10px;right: 10px;">X</span>
                    </div>`;


        // Chamada da requisição
        $.ajax({
            url: '//api.vtexcrm.com.br/'+jsnomeLoja+'/dataentities/'+MD_Entity+'/documents',
            type: 'POST',
            dataType: 'json',
            data: jsonData,
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json'
            },
            beforeSend: function() {
                $('.modal-form-button').attr("disabled", "true");
                $('.modal-form-button').text('Carregando...');
                $('.modal-form-button').css("backgroundColor", '#af4b4b');
            },
            success: function(response) {
                $('.modal-container').hide();
                $(".backgroundModal").css("display", "block");
                $('.backgroundModal').append(alert);
                $("#alert-modal").append("Dados enviados com sucesso!");

            },
            error: function(e){
                $('modal-container').hide();
                $(".backgroundModal").css("display", "block");
                $('.backgroundModal').append(alert);
                $("#alert-modal").append("Erro ao enviar os dados.");
            }
            
        });
       
        return false;
    });
    $('body').on('click', '.close', function(){
        $(".backgroundModal").css("display", "none");
    });
});