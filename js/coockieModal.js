

 $(window).load(function () {

    function checkCookieModalPrivacidade() {
        var modalPrivacidade = getCookie("modalPrivacidade");
        if (modalPrivacidade != "") {
            $('.modal-privacidade').hide();
        } else {
            $('.modal-privacidade').show();
            if (modalPrivacidade != "" && modalPrivacidade != null) {
                setCookie("modalPrivacidade", modalPrivacidade, 30);
            }
        }
    }
     checkCookieModalPrivacidade();

     $('.modal-privacidade a').on('click', function () {
         $('.modal-privacidade').hide();
     })
 });