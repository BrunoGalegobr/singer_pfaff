const YPlaylist = {
    init(config) {
        this.url       = `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=${config.playlist}&key=${config.apiKey}&callback=?`;
        this.container = config.container;
        this.shuffle   = config.shuffle;
        this.fetch();
    },
    fetch() {
        const self        = this;
        const carousel    = $('<div class="slick-track container-play-list" style="opacity: 1; width: 1208px; transform: translate3d(0px, 0px, 0px);"></div>');

        $.getJSON(self.url, data => {
            let list    = "";
            let res     = {};
            const entries = data.items;
            let i;
            let len;

            if (self.shuffle) {
                entries.sort(() => 0.5 - Math.random());
            }

            for (i = 0, len = 4; i < len; i += 1) {
                res = {
                    title: entries[i].snippet.title,
                    url: entries[i].snippet.resourceId.videoId,
                    thumb: entries[i].snippet.thumbnails.medium.url,
                    desc: entries[i].snippet.description
                };

				list += '<div class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 302px;"><div>';
				list += '<div class="slider__slide" style="width: 100%; display: inline-block;"><div class="video-preview">';
				list += `<div class="video__media" style="background-image: url(${res.thumb});"></div><!-- /.video__media -->`;
				list += `<div class="video__content"><h5 class="video__title">${res.title}</h5></div><!-- /.video__content -->`;
				list += `<a href="https://www.youtube.com/watch?v=${res.url}" class="btn btn--width btn--pink btn--pink-solid btn--ico video__btn" tabindex="0">`;
				list += '<i class="ico-play-alt"></i><span>Ver no Youtube</span></a>';
				list += '</div><!-- /.video-preview --></div></div></div>';
				
            }

			$(self.container).append(carousel);
            $(list).appendTo('.container-play-list');
        });
    },
    truncate(text, limit) {
        let last;
        if (text.length > limit) {
            limit--;
            last = text.substr(limit - 1, 1);
            while (last !== ' ' && limit > 0) {
                limit--;
                last = text.substr(limit - 1, 1);
            }
            last = text.substr(limit - 2, 1);
            if (last === ',' || last === ';'  || last === ':') {
                text = `${text.substr(0, limit - 2)}...`;
            } else if (last === '.' || last === '?' || last === '!') {
                text = text.substr(0, limit - 1);
            } else {
                text = `${text.substr(0, limit - 1)}...`;
            }
        }
        return text;
    },
   getId(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
        const match = url.match(regExp);

        if (match && match[2].length === 11) {
            return match[2];
        } else {
            throw new Error("Invalid video URL");
        }
    },
};
